$(function(){
  /*if ($(window).width() > 991) {
    $(".workarea-block_cnts").css("height",($(window).height()-166) +"px");
  }*/

  /*Main navigation
   ---------------------------------------------------------------------*/
  $(document).on("click",".nav-parent a",function(ev){
    if($("#wrapper").hasClass("sidebar-collapsed")) {
      ev.preventDefault();
    }
    else {
      $(this).parent().addClass("collapsed");
      $(this).next(".nav-children_ul").slideToggle();
    }
  });
  $(document).on("click",".collapsed a",function(){
    $(this).parent().removeClass("collapsed");
  });

  /*Main mobile-navigation
   ---------------------------------------------------------------------*/
  $(document).on("click",".navigation-mobile-parent a",function(ev){
    $(this).parent().addClass("collapsed");
    $(this).next(".navigation-mobile-children_ul").slideToggle();
  });
  $(document).on("click",".collapsed a",function(){
    $(this).parent().removeClass("collapsed");
  });

  /* Toggle mobile-navigation */
  $(document).on("click",".top-bar-mobile_toggler a",function(){
    $(this).parent().addClass("menu-collapsed");
    $(this).parent().parent().next().slideToggle();
  });

  $(document).on("click",".menu-collapsed a",function(){
    $(this).parent().removeClass("menu-collapsed");
  });

  /* Scrollbar
   ---------------------------------------------------------------------*/


  /* functionList
   ---------------------------------------------------------------------*/
  checkboxesInTable();

  $(document).on("click",".change-lng_li",function(){
    $(this).parent(".change-lng_ul").find("li").removeClass("active");
    $(this).addClass("active")

  });
});

$(window).resize(function(){
  /*if ($(window).width() > 991) {
    $(".workarea-block_cnts").css("height",($(window).height()-96) +"px");
    $(".workarea-block_filter").css("height",($(window).height()-96) +"px");
  }*/
});

function checkboxesInTable() {
  /* checkAll click
   ---------------------------------------------------------------------*/
  $(document).on("click","#checkall",function(){
    $(".table-crm input[type=checkbox]").removeClass("all-checked").removeClass("chkb-checked").prop("checked",false);
    $(".table-crm .__ttip").hide();
    $(this).addClass("all-checked");
    $(this).closest(".table-crm").find("input[type=checkbox]").prop("checked",true);
    $(this).parent().find(".__ttip").show();
  });
  $(document).on("click",".all-checked",function(){
    $(".table-crm input[type=checkbox]").removeClass("all-checked").removeClass("chkb-checked").prop("checked",false);
    $(".table-crm .__ttip").hide();
  });
  <!-- /checkAll click -->

  /* checkbox click
   ---------------------------------------------------------------------*/
  $(document).on("click",".check-this",function(){
    if($("#checkall").hasClass("all-checked")) {
      $(".table-crm input[type=checkbox]").removeClass("all-checked").removeClass("chkb-checked").prop("checked",false);
      $(".table-crm .__ttip").hide();
      $(this).addClass("chkb-checked");
      $(this).prop("checked", true);
      $(this).parent().find(".__ttip").show();
    }
    else {
      $(".table-crm .__ttip").hide();
      $(this).addClass("chkb-checked");
      $(this).prop("checked", true);
      $(this).parent().find(".__ttip").show();
    }
  });
  $(document).on("click",".chkb-checked",function(){
    if($("#checkall").hasClass("all-checked")) {
      $(".table-crm input[type=checkbox]").removeClass("all-checked").removeClass("chkb-checked").prop("checked",false);
      $(".table-crm .__ttip").hide();
    }
    $(this).removeClass("chkb-checked").prop("checked",false);
    $(this).parent().find(".__ttip").hide();
  });
  <!-- /checkbox click -->

  /* Table settings draggable select options
   ---------------------------------------------------------------------*/

  $(document).on("click","#add_col", function(){
    var selected_el = $("#firstSelect").val();
    if (selected_el.length > 0) {
      selected_el.forEach(function(el){
        $("#firstSelect option[value="+el+"]").clone().appendTo("#secondSelect");
        $("#firstSelect option[value="+el+"]").remove();
      });
    }
  });

  $(document).on("click","#rem_col", function(){
    var selected_el2 = $("#secondSelect").val();
    if (selected_el2.length > 0) {
      selected_el2.forEach(function(el){
        $("#secondSelect option[value="+el+"]").clone().appendTo("#firstSelect");
        $("#secondSelect option[value="+el+"]").remove();
      });
    }
  });

  $(document).on("click","#up", function() {
    var $selected_first;
    $($("#secondSelect option").get().reverse()).each(function () {
      var $selected = $(this).prop('selected');

      if (!$selected_first) {
        if ($selected) {
          $selected_first = $(this);
        }
        else {
          return;
        }
      }
      else {
        if ($selected) {
          return;
        }
        else {
          var $not_selected = $(this).detach();
          $not_selected.insertAfter($selected_first);
          $selected_first = null;
        }
      }
    });
  });

  $(document).on("click","#down", function() {
    var $selected_first;
    $("#secondSelect option").each(function () {
      var $selected = $(this).prop('selected');

      if (!$selected_first) {
        if ($selected) {
          $selected_first = $(this);
        }
        else {
          return;
        }
      }
      else {
        if ($selected) {
          return;
        }
        else {
          var $not_selected = $(this).detach();
          $not_selected.insertBefore($selected_first);
          $selected_first = null;
        }
      }
    });
  });
}